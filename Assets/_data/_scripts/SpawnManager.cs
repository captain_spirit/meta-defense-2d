using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour
{
    // Heroes prefabs to summon
    public GameObject commonHero;
    public GameObject uncommonHero;
    public GameObject rareHero;
    public GameObject epicHero;
    public GameObject legendHero;

    // Spawn Buttons
    public Button btnCommonSpawner;
    public Button btnUncommonSpawner;
    public Button btnRareSpawner;
    public Button btnEpicSpawner;
    public Button btnLegendSpawner;
    // Start is called before the first frame update
    void Start()
    {
        btnCommonSpawner.onClick.AddListener(SpawnCommonHero);

        btnUncommonSpawner.onClick.AddListener(SpawnUncommonHero);

        btnRareSpawner.onClick.AddListener(SpawnRareHero);

        btnEpicSpawner.onClick.AddListener(SpawnEpicHero);

        btnLegendSpawner.onClick.AddListener(SpawnLegendHero);
    }


    void SpawnCommonHero()
    {
        GameObject player = GameObject.Find("Player");
        Instantiate(
            commonHero,
            player.transform.position,
            Quaternion.identity);
    }

    void SpawnUncommonHero()
    {
        GameObject player = GameObject.Find("Player");
        Instantiate(
            uncommonHero,
            player.transform.position,
            Quaternion.identity);
    }

    void SpawnRareHero()
    {
        GameObject player = GameObject.Find("Player");
        Instantiate(
            rareHero,
            player.transform.position,
            Quaternion.identity);
    }

    void SpawnEpicHero()
    {
        GameObject player = GameObject.Find("Player");
        Instantiate(
            epicHero,
            player.transform.position,
            Quaternion.identity);
    }

    void SpawnLegendHero()
    {
        GameObject player = GameObject.Find("Player");
        Instantiate(
            legendHero,
            player.transform.position,
            Quaternion.identity);
    }
}
