using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameDev.Scripts
{
    public class BombController : MonoBehaviour
    {
        public int givingDamageDelay = 200;
        private int GivingDamageTimer
        {
            get => _givingDamageTimer;
            set => _givingDamageTimer = Mathf.Clamp(value, 0, givingDamageDelay);
        }
        private int _givingDamageTimer;
        private int _damage;
        private readonly List<GameObject> _colliders = new List<GameObject>();

        private void Awake()
        {
            Destroy(gameObject, 2);
        }

        private void Update()
        {
            GivingDamageTimer += Mathf.RoundToInt(Time.deltaTime * 1000);
            if (GivingDamageTimer < givingDamageDelay) return;
            GetComponent<SphereCollider>().enabled = false;
            enabled = false;
            GiveDamage();
        }

        private void OnCollisionEnter(Collision collision)
        {
            var other = collision.gameObject;
            if (other.gameObject == null) return;
            if (other.CompareTag("Protected") || other.CompareTag("Hostile"))
            {
                _colliders.Add(other);
            }
        }

        private void GiveDamage()
        {
            foreach (var o in _colliders)
            {
                if (o.CompareTag("Protected"))
                {
                    o.GetComponent<PostController>().TakeDamage(_damage);
                }
                else
                {
                    o.GetComponent<EnemyController>().TakeDamage(_damage);
                }
            }
        }

        public void SetDamage(int value)
        {
            _damage = value;
        }
    }
}