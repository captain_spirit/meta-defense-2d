using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TeammateController : MonoBehaviour
{
    [Header("Attack")]
        public bool canAttack;
        public GameObject bullet;
        public int attack = 10;
        public int attackRange = 5;
        public int attackThreshold = 1000;
        private int AttackTimer
        {
            get => _attackTimer;
            set => _attackTimer = Mathf.Clamp(value, 0, attackThreshold);
        }
        private int _attackTimer;
        private GameObject _target;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Attack()
        {
            // Find nearest enemy
            if (_target == null || !_target.activeSelf)
            {
                _target = FindNearestWithTag(transform.position, "Hostile");
            }

            // Shoot bullet at that enemy
            // if (_target == null) return;
            // var bulletInstance = Instantiate(bullet, transform.position, Quaternion.identity);
            // bulletInstance.GetComponent<PostBulletController>().SetTarget(_target.transform.position, attack);
        }

        private GameObject FindNearestWithTag(Vector3 origin, string tagName)
        {
            var gameObjects = GameObject.FindGameObjectsWithTag(tagName);
            if (gameObjects!.Length <= 0) return null;
            var distances = new float[gameObjects.Length];
            for (var i = 0; i < gameObjects.Length; i++)
            {
                distances[i] = (gameObjects[i].transform.position - origin).sqrMagnitude;
            }
            Array.Sort(distances, gameObjects);
            return (distances[0] < attackRange) ? gameObjects[0] : null;
        }
        private void OnCollisionEnter(Collision collision)
        {
            // Ignore player collision
            if (collision.gameObject.CompareTag("Player"))
            {
                Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
            }
        }
         
}
