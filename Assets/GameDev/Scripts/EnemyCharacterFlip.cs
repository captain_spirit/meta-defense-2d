﻿using UnityEngine;

namespace GameDev.Scripts
{
    /// <summary>
    /// Makes character to look at target (flip by X scale).
    /// </summary>
    public class EnemyCharacterFlip : MonoBehaviour
    {
	    private Transform _target;

        public void Update()
        {
	        var scale = transform.localScale;

	        scale.x = Mathf.Abs(scale.x);

	        if (_target.position.x < transform.position.x) scale.x *= -1;

			transform.localScale = scale;
        }

        public void SetTarget(Transform target)
        {
	        _target = target;
        }
    }
}