using System;
using UnityEngine;

namespace GameDev.Scripts
{
    public class EnemyBulletController : MonoBehaviour
    {
        public float speed = 4;
        public int lifetime = 5000;
        private Vector3 _targetDirection;
        private int _attack;

        public void SetTarget(Vector3 target, int attack)
        {
            _targetDirection = (target - transform.position).normalized;
            _attack = attack;
        }

        private void Awake()
        {
            Destroy(gameObject, lifetime * 1f / 1000);
        }

        private void Update()
        {
            transform.position += _targetDirection * speed * Time.deltaTime;
        }

        private void OnTriggerEnter(Collider other)
        {
            var postController = other.gameObject.GetComponent<PostController>();
            if (postController == null) return;
            postController.TakeDamage(_attack);
            Destroy(gameObject);
        }
    }
}