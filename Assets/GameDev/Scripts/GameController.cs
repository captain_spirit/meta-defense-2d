using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using GameDev.Scripts.Modal;
using UnityEngine;
using Object = UnityEngine.Object;
using UnityEngine.SceneManagement;
namespace GameDev.Scripts
{
    public static class GameController
    {
        public static int Level
        {
            get => _level;
            private set => _level = Mathf.Clamp(value, 0, 2);
        }
        private static int _level;
        public static int Coin;
        public static Player Player;

        public static bool IsPaused { get; private set; }

        private static GameObject _level0;
        private static GameObject _level1;
        private static GameObject _level2;

        public static void Pause()
        {
            Time.timeScale = 0;
            IsPaused = true;
        }

        public static void Resume()
        {
            Time.timeScale = 1;
            IsPaused = false;
        }

        public static void Save()
        {
            // Init file locations
            var pathBin = Path.Combine(Application.persistentDataPath, "/save.dat");
            var fileBin = File.Create(pathBin);

            // Get data
            var position = GameObject.FindWithTag("Player").transform.position;
            var playerData = new Player
            {
                x = position.x,
                y = position.y
            };
            var statusData = new Status
            {
                coin = Coin,
                level = Level
            };
            var saveData = new SaveData(playerData, statusData);

            // Write data to binary file
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileBin, saveData);
            fileBin.Close();
            Debug.Log("Game saved to " + pathBin);
        }

        public static void Load()
        {
            // Check file exists
            var pathBin = Path.Combine(Application.persistentDataPath, "/save.dat");
            if (!File.Exists(pathBin)) return;

            // Read data from binary file
            var fileBin = File.Open(pathBin, FileMode.Open);
            var binaryFormatter = new BinaryFormatter();
            var saveData = (SaveData)binaryFormatter.Deserialize(fileBin);
            fileBin.Close();

            // Set data
            Player = saveData.player;
            Coin = saveData.status.coin;
            Level = saveData.status.level;
            StartLevel();
        }

        public static void StartLevel()
        {
            // Teleport player & enable map
            if (_level0 == null)
            {
                _level0 = GameObject.Find("Level0");
                _level1 = GameObject.Find("Level1");
                _level2 = GameObject.Find("Level2");
            }
            var player = GameObject.FindWithTag("Player");
            var position = Vector3.zero;
            switch (Level)
            {
                case 0:
                    position = new Vector3(2, 1.5f, 0);
                    _level0.SetActive(true);
                    _level1.SetActive(false);
                    _level2.SetActive(false);
                    break;
                case 1:
                    position = new Vector3(104, 1.5f, 0);
                    _level0.SetActive(false);
                    _level1.SetActive(true);
                    _level2.SetActive(false);
                    break;
                case 2:
                    position = new Vector3(214, 1.5f, 0);
                    _level0.SetActive(false);
                    _level1.SetActive(false);
                    _level2.SetActive(true);
                    break;
                case 3:
                    _level0.SetActive(false);
                    _level1.SetActive(false);
                    _level2.SetActive(false);
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
                    break;
            }
            player.transform.position = position;

            // Clean everything
            foreach (var enemy in GameObject.FindGameObjectsWithTag("Hostile"))
            {
                Object.Destroy(enemy);
            }
        }

        public static void NextLevel()
        {
            Level++;
            Save();
            StartLevel();
        }
    }
}