using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class menuShop : MonoBehaviour
{
    public List<string> TypeChar = new List<string>();
    int i,x,y;
    public GameObject txt1,txt2,txt3;
    public GameObject shopOpen;
    bool isOpen;
    void Start()
    {
        isOpen = false;
        i = 0;
        x = 1;
        y = 2;
        TypeChar.Add("Common");
        TypeChar.Add("Rare");
        TypeChar.Add("Super Rare");
        TypeChar.Add("Epic");
        TypeChar.Add("Legend");
        txt1.GetComponent<TextMeshProUGUI>().text = TypeChar[i];
        txt2.GetComponent<TextMeshProUGUI>().text = TypeChar[x];
        txt3.GetComponent<TextMeshProUGUI>().text = TypeChar[y];
    }
    public void OpenShop(){
        if(!isOpen){
            shopOpen.gameObject.SetActive(true);
            isOpen = true;}
        else if(isOpen){
            shopOpen.gameObject.SetActive(false);
            isOpen = false;
        }
    }
    public void onNext(){
        i++;
        x++;
        y++;
        if(i<0){
            i=4;
        }
        if(i>4){
            i=0;
        }
        if(x<0){
            x=4;
        }
        if (x>4){
            x=0;
        }
        if(y<0){
            y=4;
        }
        if(y>4){
            y=0;
        }
        txt1.GetComponent<TextMeshProUGUI>().text = TypeChar[i];
        txt2.GetComponent<TextMeshProUGUI>().text = TypeChar[x];
        txt3.GetComponent<TextMeshProUGUI>().text = TypeChar[y];
    }
    public void onPrevious(){
        i--;
        x--;
        y--;
        if(i<0){
            i=4;
        }
        if(i>4){
            i=0;
        }
        if(x<0){
            x=4;
        }
        if (x>4){
            x=0;
        }
        if(y<0){
            y=4;
        }
        if(y>4){
            y=0;
        }
        txt1.GetComponent<TextMeshProUGUI>().text = TypeChar[i];
        txt2.GetComponent<TextMeshProUGUI>().text = TypeChar[x];
        txt3.GetComponent<TextMeshProUGUI>().text = TypeChar[y];
    }
    bool checkMoney(){
        if (GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin <= 0){
            return false;
        }else{
            return true;
        }
    }
    public void BuySlot1(){
        if(i==0){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnCommonHero();
            } 
        }else if(i==1){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnUncommonHero();
            } 
        }else if(i==2){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                 GetComponent<SpawnTeammate>().SpawnRareHero();
            } 
        }else if (i==3){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnEpicHero();
            } 
        }else if(i==4){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnLegendHero();
            } 
        }
    }
    public void BuySlot2(){
        if(x==0){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnCommonHero();
            } 
        }else if(x==1){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnUncommonHero();
            } 
        }else if(x==2){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                 GetComponent<SpawnTeammate>().SpawnRareHero();
            } 
        }else if (x==3){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnEpicHero();
            } 
        }else if(x==4){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnLegendHero();
            } 
        }
    }
    public void BuySlot3(){
        if(y==0){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnCommonHero();
            } 
        }else if(y==1){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnUncommonHero();
            } 
        }else if(y==2){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                 GetComponent<SpawnTeammate>().SpawnRareHero();
            } 
        }else if (y==3){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnEpicHero();
            } 
        }else if(y==4){
            if(checkMoney()){
                GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin -= 50;
                GetComponent<SpawnTeammate>().SpawnLegendHero();
            } 
        }
    }
}
