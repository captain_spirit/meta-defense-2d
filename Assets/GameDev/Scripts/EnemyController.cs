using System;
using System.Collections.Generic;
using System.Linq;
using Assets.HeroEditor.Common.CharacterScripts;
using HeroEditor.Common.Enums;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameDev.Scripts
{
    public class EnemyController : MonoBehaviour
    {
        [Header("Bounty")]
        public int coinDropAmount;

        [Header("Health")]
        public int maxHealth = 100;
        public RectTransform healthBar;
        private int Health
        {
            get => _health;
            set => _health = Mathf.Clamp(value, 0, maxHealth);
        }
        private int _health;
        private float _healthBarWidth;
        private bool _dying;

        [Header("Armor")]
        public bool randomizedArmor;
        public int maxArmor;
        [Range(0, 100)]
        public int headGearStrength = 50;
        [Range(0, 100)]
        public int upperBodyStrength = 30;
        [Range(0, 100)]
        public int lowerBodyStrength = 20;

        [Header("Attack")]
        public int attack = 50; // Attack damage
        public float attackDistance = 2f; // Distance to start attacking
        public int attackThreshold = 1000; // Delay between attacks

        [Header("Attack variants")]
        public AttackType attackType = AttackType.Melee;
        public enum AttackType { Melee, Bomb, Ranged, Summon }
        public GameObject attackBombObject; // Explosion GameObject
        public GameObject attackRangedObject; // Bullet GameObject
        public int attackRangedRate; // Amount of bullets to shoot
        public GameObject attackSummonObject; // GameObject to spawn
        public int attackSummonRate; // Amount of GameObjects to spawn
        private int AttackTimer
        {
            get => _attackTimer;
            set => _attackTimer = Mathf.Clamp(value, 0, attackThreshold);
        }
        private int _attackTimer;

        [Header("Movement")]
        public float speed = 4;
        public GameObject target;
        public ParticleSystem trailParticleSystem;

        private Character _character;
        private EnemyCharacterFlip _enemyCharacterFlip;
        private EnemyWeaponControls _enemyWeaponControls;

        private void Awake()
        {
            // Auto-detect target in current scene
            if (target == null)
            {
                target = FindNearestWithTag(transform.position, "Protected");
            }

            // Get components
            _character = GetComponent<Character>();
            _enemyCharacterFlip = GetComponent<EnemyCharacterFlip>();
            _enemyCharacterFlip.SetTarget(target.transform);
            _enemyWeaponControls = GetComponent<EnemyWeaponControls>();
            _enemyWeaponControls.SetTarget(target.transform);

            // Randomly remove armor and update Health
            if (randomizedArmor)
            {
                maxHealth += maxArmor;
                if (Random.Range(0, 3) != 0)
                {
                    _character.Helmet = null;
                    _character.HelmetRenderer.sprite = _character.Helmet;
                    maxHealth -= maxArmor * headGearStrength / 100;
                    Debug.Log($"Removed head gear from {name}");
                }
                if (Random.Range(0, 3) != 0)
                {
                    for (var i = 0; i < 6; i++)
                    {
                        _character.Armor[i] = null;
                        _character.ArmorRenderers[i].sprite = null;
                    }
                    maxHealth -= maxArmor * upperBodyStrength / 100;
                    Debug.Log($"Removed upper body from {name}");
                }
                if (Random.Range(0, 3) != 0)
                {
                    for (var i = 6; i < 10; i++)
                    {
                        _character.Armor[i] = null;
                        _character.ArmorRenderers[i].sprite = null;
                    }
                    maxHealth -= maxArmor * lowerBodyStrength / 100;
                    Debug.Log($"Removed lower body from {name}");
                }
                _character.Initialize();
            }
            Health = maxHealth;
            _healthBarWidth = healthBar.sizeDelta.x;

            // Rotate trail particle system
            Vector3 tfSelf = transform.position, tfTarget = target.transform.position;
            if (trailParticleSystem != null && tfSelf.x > tfTarget.x)
            {
                trailParticleSystem.GetComponentInChildren<Transform>().rotation = new Quaternion(0, 180, 0, 0);
            }
        }

        private void Update()
        {
            // Update vars
            AttackTimer -= Mathf.RoundToInt(Time.deltaTime * 1000);

            // Update HUD
            var healthBarSize = healthBar.sizeDelta;
            healthBarSize.x = _healthBarWidth * Health / maxHealth;
            healthBar.sizeDelta = healthBarSize;

            // Die event (on health empty or falling)
            if (Health <= 0 || transform.position.y < -20)
            {
                if (_dying) return;
                Die();
                return;
            }

            // Move towards the post
            Vector3 tfSelf = transform.position, tfTarget = target.transform.position;
            if (Vector3.Distance(tfSelf, tfTarget) > attackDistance)
            {
                var targetX = tfSelf.x < tfTarget.x
                    ? tfTarget.x + attackDistance
                    : tfTarget.x - attackDistance;
                tfTarget = new Vector2(targetX, tfSelf.y);
                transform.position = Vector2.MoveTowards(
                    tfSelf, tfTarget,
                    speed * Time.deltaTime);
            }

            // Attack the post if enemy is near
            else
            {
                if (trailParticleSystem != null)
                {
                    trailParticleSystem.Stop();
                }

                if (AttackTimer <= 0)
                {
                    Attack();
                }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            // Ignore player & enemy collision
            var colliderObject = collision.gameObject;
            if (colliderObject.CompareTag("Player") || colliderObject.CompareTag("Hostile"))
            {
                Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
                return;
            }

            // Bullet collision
            if (colliderObject.GetComponent<Shell>() != null
                || colliderObject.GetComponent<Projectile>() != null)
            {
                TakeDamage(10);
                Destroy(colliderObject);
            }
        }

        private GameObject FindNearestWithTag(Vector3 origin, string tagName)
        {
            var gameObjects = GameObject.FindGameObjectsWithTag(tagName);
            if (gameObjects!.Length <= 0) return null;
            var distances = new float[gameObjects.Length];
            for (var i = 0; i < gameObjects.Length; i++)
            {
                distances[i] = (gameObjects[i].transform.position - origin).sqrMagnitude;
            }
            Array.Sort(distances, gameObjects);
            return gameObjects[0];
        }

        private void Attack()
        {
            // Make the target take damage
            var controller = target.GetComponent<PostController>();
            switch (attackType)
            {
                // Melee: Take damage right away
                case AttackType.Melee:
                    controller.TakeDamage(attack);
                    break;

                // Charge: Charged attack then melee
                case AttackType.Bomb:
                    Die();
                    break;

                // Ranged: Shoot a projectile and let it invokes damage
                case AttackType.Ranged:
                    Vector3 tfSelf = transform.position, tfTarget = target.transform.position;
                    var bulletInstance = Instantiate(attackRangedObject,
                        tfSelf + Vector3.up * 2.4f + Vector3.back * 10, Quaternion.identity);
                    if (tfSelf.x > tfTarget.x)
                    {
                        bulletInstance.GetComponentInChildren<Transform>().rotation = new Quaternion(0, 180, 0, 0);
                    }
                    bulletInstance.GetComponent<EnemyBulletController>().SetTarget(tfTarget + Vector3.back * .1f, attack);
                    break;

                // Summon a pack of predefined enemies
                case AttackType.Summon:
                    var position = transform.position + Vector3.up * .25f;
                    var positionLeft = position + Vector3.left * 2f + Vector3.up * .25f;
                    var positionRight = position + Vector3.right * 2f + Vector3.up * .25f;
                    Instantiate(attackSummonObject, position, Quaternion.identity);
                    Instantiate(attackSummonObject, positionLeft, Quaternion.identity);
                    Instantiate(attackSummonObject, positionRight, Quaternion.identity);
                    Debug.Log($"Spawned 3 {attackSummonObject.name} at {position}");
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            // Start animation & restart timer
            _enemyWeaponControls.Fire();
            AttackTimer = attackThreshold;
        }

        public void TakeDamage(int damage)
        {
            Health -= damage;
        }

        private void Die()
        {
            // Disable events and HUD
            _dying = true;
            _character.enabled = false;
            GetComponentInChildren<Canvas>().enabled = false;

            // Check if attack type is Bomb -> Spawn explosion
            if (attackType == AttackType.Bomb)
            {
                var ex = Instantiate(attackBombObject, transform.position, Quaternion.identity);
                ex.GetComponent<BombController>().SetDamage(attack);
            }

            // Drop head out of screen
            var helmet = _character.Helmet;
            _character.ResetEquipment();
            _character.EquipHelmet(helmet);
            _character.Body.Clear();
            _character.Initialize();
            GetComponent<Collider>().enabled = false;
            Destroy(gameObject, 2);

            // Add coin to player
            GameObject.Find("Cyperpunk01").GetComponent<CoinSystem>().coin += coinDropAmount;
        }
    }
}