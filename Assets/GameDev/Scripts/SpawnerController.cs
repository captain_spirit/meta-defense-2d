using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameDev.Scripts
{
    public class SpawnerController : MonoBehaviour
    {
        [Header("Spawning")]
        public int spawnThreshold = 10000;
        public bool randomizedThreshold = true;
        public int randomizedValue = 2000;
        public List<GameObject> commonEnemies = new List<GameObject>();
        [Range(0, 100)]
        public int commonChance = 40;
        public List<GameObject> uncommonEnemies = new List<GameObject>();
        [Range(0, 100)]
        public int uncommonChance = 30;
        public List<GameObject> rareEnemies = new List<GameObject>();
        [Range(0, 100)]
        public int rareChance = 15;
        public List<GameObject> epicEnemies = new List<GameObject>();
        [Range(0, 100)]
        public int epicChance = 10;
        public List<GameObject> legendaryEnemies = new List<GameObject>();
        [Range(0, 100)]
        public int legendaryChance = 5;
        private int SpawnTimer
        {
            get => _spawnTimer;
            set => _spawnTimer = Mathf.Clamp(value, 0, _spawnMark);
        }
        private int _spawnTimer, _spawnMark;

        private static readonly System.Random Rand = new System.Random();

        [Header("Appearance")]
        public bool floats = true;
        private Vector3 _originalPosition;

        private void Start()
        {
            _spawnMark = spawnThreshold + Random.Range(-randomizedValue, randomizedValue + 1);
            SpawnTimer = _spawnMark;
            _originalPosition = transform.position;
        }

        private void Update()
        {
            // Update vars
            SpawnTimer -= Mathf.RoundToInt(Time.deltaTime * 1000);

            // Spawn event
            if (SpawnTimer <= 0)
            {
                Spawn();
            }

            // Appearance animations
            if (!floats) return;
            var pos = transform.position;
            pos.y = _originalPosition.y + .25f * Mathf.Sin(Time.time);
            transform.position = pos;
        }

        private void Spawn()
        {
            // Pick a rarity from list
            var rarityList = ComputeRarity();
            var chanceRemaining = 0;
            for (var i = rarityList.Count - 1; i >= 0; i--)
            {
                if (rarityList[i] == 0) continue;
                var chance = rarityList[i];
                var value = Random.Range(0, 100);
                if (value < rarityList[i] + chanceRemaining)
                {
                    SpawnFromIndex(i);
                    break;
                }
                chanceRemaining += chance;
            }

            // Reset spawn timer
            _spawnMark = spawnThreshold + (randomizedThreshold
                ? Random.Range(-randomizedValue, randomizedValue + 1)
                : 0);
            SpawnTimer = _spawnMark;
            Debug.Log($"Spawn timer reset to {_spawnMark}ms.");
        }

        private void SpawnFromIndex(int index)
        {
            // Pick rarity list from index
            var list = index switch
            {
                0 => commonEnemies,
                1 => uncommonEnemies,
                2 => rareEnemies,
                3 => epicEnemies,
                4 => legendaryEnemies,
                _ => null
            };

            // Pick a random enemy from list
            if (list == null) return;
            var i = Rand.Next(list.Count);

            // Spawn it
            var position = transform.position;
            Instantiate(list[i], position, Quaternion.identity);
            Debug.Log($"Spawned {list[i].name} at {position}");
        }

        private List<int> ComputeRarity()
        {
            // Create list of rarity ranks with their corresponding chance
            var list = new List<int>
            {
                commonEnemies.Count > 0    ? commonChance : 0,
                uncommonEnemies.Count > 0  ? uncommonChance : 0,
                rareEnemies.Count > 0      ? rareChance : 0,
                epicEnemies.Count > 0      ? epicChance : 0,
                legendaryEnemies.Count > 0 ? legendaryChance : 0
            };
            var emptyCount = list.FindAll(i => i == 0).Count;
            if (emptyCount == 0 || emptyCount == 5) return list;

            // Distribute remaining chances to available lists (due to some lists being empty)
            var chanceRemaining = 100 - list.Sum();
            var changeDistribution = chanceRemaining / (5 - emptyCount);
            for (var i = 0; i < list.Count; i++)
            {
                if (list[i] > 0)
                {
                    list[i] += changeDistribution;
                }
            }

            return list;
        }
    }
}