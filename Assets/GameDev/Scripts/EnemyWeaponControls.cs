﻿using System;
using Assets.HeroEditor.Common.CharacterScripts;
using HeroEditor.Common.Enums;
using UnityEngine;

namespace GameDev.Scripts
{
    /// <summary>
    /// Rotates arms and passes events to child components like FirearmFire and BowShooting.
    /// </summary>
    public class EnemyWeaponControls : MonoBehaviour
    {
        public Character Character;
        public Transform ArmL;
        public Transform ArmR;
	    public bool FixHorizontal;

        private bool _locked;
        private Transform _target;

        private bool Firing
        {
            get => _firing;
            set
            {
                _firing = value;
                _seizingFire = !value;
            }
        }
        private bool _firing, _seizingFire;

        private bool Reloading { get; set; }


        public void Update()
        {
            _locked = !Character.Animator.GetBool("Ready") || Character.Animator.GetInteger("Dead") > 0;

            if (_locked) return;

            switch (Character.WeaponType)
            {
                case WeaponType.Melee1H:
                case WeaponType.Melee2H:
                case WeaponType.MeleePaired:
                    if (Firing)
                    {
                        Character.Animator.SetTrigger(Time.frameCount % 2 == 0 ? "Slash" : "Jab"); // Play animation randomly
                    }
                    break;
                case WeaponType.Bow:
                    Character.BowShooting.ChargeButtonDown = Firing;
                    Character.BowShooting.ChargeButtonUp = _seizingFire;
                    break;
                case WeaponType.Firearms1H:
                case WeaponType.Firearms2H:
                    Character.Firearm.Fire.FireButtonDown = Firing;
                    Character.Firearm.Fire.FireButtonPressed = Firing;
                    Character.Firearm.Fire.FireButtonUp = _seizingFire;
                    Character.Firearm.Reload.ReloadButtonDown = Reloading;
                    break;
	            case WeaponType.Supplies:
		            if (Firing)
		            {
			            Character.Animator.Play(Time.frameCount % 2 == 0 ? "UseSupply" : "ThrowSupply", 0); // Play animation randomly
		            }
		            break;
			}

            // Stop attack during cooldown
            Firing = false;
        }

        /// <summary>
        /// Called each frame update, weapon to mouse rotation example.
        /// </summary>
        public void LateUpdate()
        {
            if (_locked) return;

            Transform arm;
            Transform weapon;

            switch (Character.WeaponType)
            {
                case WeaponType.Bow:
                    arm = ArmL;
                    weapon = Character.BowRenderers[3].transform;
                    break;
                case WeaponType.Firearms1H:
                case WeaponType.Firearms2H:
                    arm = ArmR;
                    weapon = Character.Firearm.FireTransform;
                    break;
                default:
                    return;
            }

            RotateArm(arm, weapon,
                FixHorizontal ? arm.position + 1000 * Vector3.right : _target.position,
                -40, 40);
        }

        /// <summary>
        /// Selected arm to position (world space) rotation, with limits.
        /// </summary>
        public void RotateArm(Transform arm, Transform weapon, Vector2 target, float angleMin, float angleMax) // TODO: Very hard to understand logic
        {
            target = arm.transform.InverseTransformPoint(target);
            
            var angleToTarget = Vector2.SignedAngle(Vector2.right, target);
            var angleToFirearm = Vector2.SignedAngle(weapon.right, arm.transform.right) * Math.Sign(weapon.lossyScale.x);
            var angleFix = Mathf.Asin(weapon.InverseTransformPoint(arm.transform.position).y / target.magnitude) * Mathf.Rad2Deg;
            var angle = angleToTarget + angleToFirearm + angleFix;

            angleMin += angleToFirearm;
            angleMax += angleToFirearm;

            var z = arm.transform.localEulerAngles.z;

            if (z > 180) z -= 360;

            if (z + angle > angleMax)
            {
                angle = angleMax;
            }
            else if (z + angle < angleMin)
            {
                angle = angleMin;
            }
            else
            {
                angle += z;
            }

            arm.transform.localEulerAngles = new Vector3(0, 0, angle);
        }

        public void SetTarget(Transform target)
        {
            _target = target;
        }

        public void Fire()
        {
            Firing = true;
        }
    
        public void Reload()
        {
            Reloading = true;
        }
    }
}