using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace GameDev.Scripts
{
public class CountTime : MonoBehaviour
{
    // Start is called before the first frame update
    public float timeRemain = 300;
    public bool isRunning = false;
    public GameObject txt;
    void Start()
    {
        isRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(isRunning){
            if(timeRemain > 0){
                timeRemain -= Time.deltaTime;
                DisplayTime(timeRemain);
            }else{
                timeRemain = 0;
                isRunning = false;
                GameController.NextLevel();
                timeRemain = 300;
                isRunning = true;
            }
        }
    }
    void DisplayTime(float time){
        time += 1;
        float minute = Mathf.FloorToInt(time/60);
        float seconds = Mathf.FloorToInt(time%60);
        txt.GetComponent<TextMeshProUGUI>().text = string.Format("{0:00}:{1:00}",minute,seconds); 
    }
}
}