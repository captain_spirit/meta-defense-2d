using System;
using UnityEngine;

namespace GameDev.Scripts
{
    public class PostBulletController : MonoBehaviour
    {
        public float speed = 4;
        public int lifetime = 2000;
        private Vector3 _targetDirection;
        private int _attack;

        public void SetTarget(Vector3 target, int attack)
        {
            _targetDirection = (target - transform.position).normalized;
            _attack = attack;
        }

        private void Awake()
        {
            Destroy(gameObject, lifetime * 1f / 1000);
        }

        private void Update()
        {
            transform.position += _targetDirection * speed * Time.deltaTime;
        }

        private void OnTriggerEnter(Collider other)
        {
            var enemyController = other.gameObject.GetComponent<EnemyController>();
            if (enemyController == null) return;
            enemyController.TakeDamage(_attack);
            Destroy(gameObject);
        }
    }
}