using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTeammate : MonoBehaviour
{
    public GameObject commonHero;
    public GameObject uncommonHero;
    public GameObject rareHero;
    public GameObject epicHero;
    public GameObject legendHero;
    void Start()
    {
        
    }

    // Update is called once per frame
    public void SpawnCommonHero()
    {
        GameObject player = GameObject.Find("Cyperpunk01");
        Instantiate(
            commonHero,
            player.transform.position,
            Quaternion.identity);
    }

    public void SpawnUncommonHero()
    {
        GameObject player = GameObject.Find("Cyperpunk01");
        Instantiate(
            uncommonHero,
            player.transform.position,
            Quaternion.identity);
    }

    public void SpawnRareHero()
    {
        GameObject player = GameObject.Find("Cyperpunk01");
        Instantiate(
            rareHero,
            player.transform.position,
            Quaternion.identity);
    }

    public void SpawnEpicHero()
    {
        GameObject player = GameObject.Find("Cyperpunk01");
        Instantiate(
            epicHero,
            player.transform.position,
            Quaternion.identity);
    }

    public void SpawnLegendHero()
    {
        GameObject player = GameObject.Find("Cyperpunk01");
        Instantiate(
            legendHero,
            player.transform.position,
            Quaternion.identity);
    }
}
