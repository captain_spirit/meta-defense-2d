using System;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
namespace GameDev.Scripts
{
    public class Testing : MonoBehaviour
    {
        public GameObject txt;
        public GameObject restart;
        public GameObject exit;
        public GameObject Post1,Post2,Post3;
        public GameObject Timer;
        private void Awake() {
            GameController.StartLevel();
        }
        private void Update()
        {
            if(Post1.GetComponent<PostController>()._dying || Post2.GetComponent<PostController>()._dying ||Post3.GetComponent<PostController>()._dying){
                txt.GetComponent<TextMeshProUGUI>().gameObject.SetActive(true);
                Timer.GetComponent<CountTime>().timeRemain = 9999999999999;
                Timer.transform.localScale = new Vector3(0,0,0);
                GameController.Pause();
            }else{
                txt.GetComponent<TextMeshProUGUI>().gameObject.SetActive(false);
                GameController.Resume();
            }
        }
        public void restartGame(){
             Scene scene = SceneManager.GetActiveScene(); 
             SceneManager.LoadScene(scene.name);
             GameController.Resume();
        }
        public void exitGame(){
            Application.Quit();
        }
    }
}