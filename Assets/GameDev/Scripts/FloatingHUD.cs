using System;
using UnityEngine;

namespace GameDev.Scripts
{
    public class FloatingHUD : MonoBehaviour
    {
        private void Update()
        {
            var cameraTransform = Camera.main!.transform;
            Transform hudTransform;
            (hudTransform = transform).LookAt(cameraTransform);
            transform.rotation = Quaternion.LookRotation(hudTransform.position - cameraTransform.position);
        }
    }
}