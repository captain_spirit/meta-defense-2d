using System;

namespace GameDev.Scripts.Modal
{
    [Serializable]
    public class SaveData
    {
        public Player player;
        public Status status;

        public SaveData(Player player, Status status)
        {
            this.player = player;
            this.status = status;
        }
    }

    [Serializable]
    public struct Player
    {
        public float x;
        public float y;
    }

    [Serializable]
    public struct Status
    {
        public int coin;
        public int level;
    }
}