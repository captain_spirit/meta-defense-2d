using System;
using UnityEngine;

namespace GameDev.Scripts
{
    public class PostController : MonoBehaviour
    {
        [Header("Health")]
        public int maxHealth = 1000;
        public bool canAutoHeal = true;
        public int healRate = 20;
        public int healThreshold = 2000;
        public RectTransform healthBar;
        private int Health
        {
            get => _health;
            set => _health = Mathf.Clamp(value, 0, maxHealth);
        }
        private int _health;
        private int HealTimer
        {
            get => _healTimer;
            set => _healTimer = Mathf.Clamp(value, 0, healThreshold);
        }
        private int _healTimer;
        private float _healthBarWidth;
        public bool _dying;

        [Header("Attack")]
        public bool canAttack;
        public GameObject bullet;
        public int attack = 10;
        public int attackRange = 5;
        public int attackThreshold = 1000;
        private int AttackTimer
        {
            get => _attackTimer;
            set => _attackTimer = Mathf.Clamp(value, 0, attackThreshold);
        }
        private int _attackTimer;
        private GameObject _target;

        [Header("Appearance")]
        public bool floats = true;
        public bool changesColor = true;
        public new ParticleSystem particleSystem;
        private Vector3 _originalPosition;
        private SpriteRenderer _renderer;

        private Color _tintStartColor, _tintEndColor;

        private void Start()
        {
            Health = maxHealth;
            HealTimer = healThreshold;
            _healthBarWidth = healthBar.sizeDelta.x;
            AttackTimer = attackThreshold;

            _originalPosition = transform.position;

            _renderer = GetComponent<SpriteRenderer>();
            _tintStartColor = new Color(1f, 1f, 138 / 256f);
            _tintEndColor = new Color(1f, 138f / 256f, 1f);
        }

        private void Update()
        {
            // Update HUD
            var healthBarSize = healthBar.sizeDelta;
            healthBarSize.x = _healthBarWidth * Health / maxHealth;
            healthBar.sizeDelta = healthBarSize;

            // TODO: Die event (needs GUI)
            if (Health <= 0 && !_dying)
            {
                Die();
            }

            // Heal self
            if (canAutoHeal)
            {
                HealTimer -= Mathf.RoundToInt(Time.deltaTime * 1000);
                if (HealTimer <= 0 && Health < maxHealth)
                {
                    Heal(healRate);
                    HealTimer = healThreshold;
                }
            }

            // Attack nearest enemy
            if (canAttack)
            {
                AttackTimer -= Mathf.RoundToInt(Time.deltaTime * 1000);
                if (AttackTimer <= 0)
                {
                    Attack();
                    AttackTimer = attackThreshold;
                }
            }

            // Appearance animations
            if (floats)
            {
                var pos = transform.position;
                pos.y = _originalPosition.y + .25f * Mathf.Sin(2 * Time.time);
                transform.position = pos;
            }

            if (changesColor)
            {
                _renderer.color =
                    Color.Lerp(_tintStartColor, _tintEndColor, Mathf.PingPong(Time.time, 1));
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            // Ignore player collision
            if (collision.gameObject.CompareTag("Player"))
            {
                Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider);
            }
        }

        private void Die()
        {
            _dying = true;
        }

        /**
         * Heal post by amount (could be triggered by player)
         */
        // ReSharper disable once MemberCanBePrivate.Global
        public void Heal(int amount)
        {
            Health += amount;
            // Debug.Log($"Post healed {amount}, current health: {Health}");
        }

        /**
         * Attack nearest enemy (could be triggered by player)
         */
        // ReSharper disable once MemberCanBePrivate.Global
        public void Attack()
        {
            // Find nearest enemy
            if (_target == null || !_target.activeSelf)
            {
                _target = FindNearestWithTag(transform.position, "Hostile");
            }

            // Shoot bullet at that enemy
            if (_target == null) return;
            var bulletInstance = Instantiate(bullet, transform.position, Quaternion.identity);
            bulletInstance.GetComponent<PostBulletController>().SetTarget(_target.transform.position, attack);
        }

        private GameObject FindNearestWithTag(Vector3 origin, string tagName)
        {
            var gameObjects = GameObject.FindGameObjectsWithTag(tagName);
            if (gameObjects!.Length <= 0) return null;
            var distances = new float[gameObjects.Length];
            for (var i = 0; i < gameObjects.Length; i++)
            {
                distances[i] = (gameObjects[i].transform.position - origin).sqrMagnitude;
            }
            Array.Sort(distances, gameObjects);
            return (distances[0] < attackRange) ? gameObjects[0] : null;
        }

        public void TakeDamage(int damage)
        {
            Health -= damage;
            particleSystem.Play();
            // Debug.Log($"Post taken damage by {damage}, remaining health: {Health}");
        }
    }
}