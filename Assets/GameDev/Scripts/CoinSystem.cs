using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CoinSystem : MonoBehaviour
{
    public int coin;
    public GameObject txt;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        txt.GetComponent<TextMeshProUGUI>().text = coin.ToString();
    }
    
}
